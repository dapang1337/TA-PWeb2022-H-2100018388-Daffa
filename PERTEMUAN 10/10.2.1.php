<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TUGAS 10.2.1</title>
    <style>
        .titleContent{
            /* border: 1px solid red;  */
            height:10vh;
            /* align :center;        */
        }
        .titleContent h1{
            /* border: 1px solid red;  */
            font-family: verdana;
            font-size: 25px;
            color: red;
            text-align: center
        }
        body {
            background-color: yellow;
        }
        form{
            font-family: verdana;
            font-size: 12px;
            color: red;
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="titleContent">
        <h1>Convert Nilai Angka ke Huruf</h1>
    </div>
    <form action="" method="post">
        Masukkan Angka :
        <input type="number" name="nilai" id="">
        <input type="submit" name="submit"value="submit">
    </form>
<?php
    if(isset($_POST['submit'])){
        $nilai = $_POST['nilai'];
        if ($nilai >= 80){
            echo "Nilai Anda : A";
        } 
        else if ($nilai >= 76.25){
            echo "Nilai Anda : A-";
        } 
        else if ($nilai >= 68.75){
            echo "Nilai Anda : B+";
        } 
        else if ($nilai >= 65.00){
            echo "Nilai Anda : B";
        } 
        else if ($nilai >= 62.50){
            echo "Nilai Anda : B-";
        } 
        else if ($nilai >= 57.50){
            echo "Nilai Anda : C+";
        } 
        else if ($nilai >= 55.00){
            echo "Nilai Anda : C";
        } 
        else if ($nilai >= 51.25){
            echo "Nilai Anda : C";
        } 
        else if ($nilai >= 43.75){
            echo "Nilai Anda : D+";
        } 
        else if ($nilai >= 40.00){
            echo "Nilai Anda : D";
        } 
        else {
            echo "Nilai Anda : E";
        }
    }
?>
</body>
</html>